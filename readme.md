# Curso de FastAPI: Fundamentos, Path Operations y Validaciones

Curso de [platzi](https://platzi.com/cursos/fundamentos-fastapi/)

## Instalation

```bash
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
```

## Execute

En la raiz del proyecto

```bash
uvicorn main:app
```
